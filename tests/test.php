
<?php
/**
 * Test taken from https://www.php.net/manual/en/random-engine.generate.php example.
 */

if ( empty( getenv( 'AQN_API_KEY' ) ) ) {
    printf( 'set AQN_API_KEY environment variable, eg. using CLI `AQN_API_KEY=<your-api-key> php82 -f %s`', basename(__FILE__ ) );
    echo "\n";
    die();
}

require dirname( __DIR__ ) . '/src/ANUQuantumRandomEngine.php';

$r = new \Random\Randomizer( new \DKZR\ANUQuantumRandomEngine( getenv( 'AQN_API_KEY' ) ) );

$i = 1;
while ( $i < 10 ) {
    // first takes a while (request to ANU takes place). After that it should go quickly
    echo "Lucky Number $i: ", $r->getInt(0, 99), "\n";
    $i++;
}
