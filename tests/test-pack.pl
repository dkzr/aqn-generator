#!/usr/bin/perl

# d65ddbe509d40000
# 8000e60100600000
# 961a91988f3c8f32

my ( $hex ) = "961a91988f3c8f32";
my ( $int ) = hex( $hex );

my ( $memb ) = `php -r 'echo hex2bin( "$hex" );'`;
my ( $meml ) = `php -r 'echo hex2bin( implode( array_reverse( str_split( "$hex", 2 ) ) ) );'`;


print "Hex: $hex => int: $int\n";

if ( pack( 'Q>', $int ) eq $memb ) {
    print "Q> (perl) = J (php) = \$memb value\n";
}
if ( pack( 'Q<', $int ) eq $meml ) {
    print "Q< (perl) = P (php) = \$meml value\n";
}
