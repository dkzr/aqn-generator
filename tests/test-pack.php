<?php
$test_data = [];

//$test_data[] = 'f000000000000000';
//$test_data[] = 'fffffffffffffff0';
//$test_data[] = '0000000000000001';
//$test_data[] = 'efffffffffffffff';
//$test_data[] = 'f431f40e16a6e979';
//$test_data[] = '1c6d02dc39dcf9e9';
//$test_data[] = '5b95ffa8fefaf24c';
//$test_data[] = '1a62fbafa9b2c281';
$test_data[] = 'ffffffffffffffff';
$test_data[] = '0000000000000000';
$test_data[] = '1f1836e001359d18';
$test_data[] = '7fffffffffffffff'; // max signed INT
$test_data[] = '8000000000000000'; // becomes float internally but is edge-case
$test_data[] = '8000000000000001'; // becomes float internally
$test_data[] = 'b55674ec347086a2';
$test_data[] = '95156c0bf37907ee';
$test_data[] = '339e787c41b8268e';

foreach ( $test_data as $hex ) {
    var_export( [
        'hex                         ' => $hex,
        'base_convert                ' => base_convert( $hex, 16, 10 ),
        'hexdec                      ' => hexdec( $hex ),
        'dechex hexdec               ' => sprintf( '%016s', dechex( (int) hexdec( $hex ) ) ), // not always correct
        'within INT limits?          ' => is_integer( hexdec( $hex ) ) ? 'yes' : 'no',
        'hex === dechex hexdec       ' => $hex === sprintf( '%016s', dechex( (int) hexdec( $hex ) ) ) ? 'yes' : 'no',
        'hex2bin === pack J          ' => hex2bin( $hex ) === pack( 'J', hexdec( $hex ) ) ? 'yes' : 'no', // works for HEX < "8000000000000000"
        'swapped hex2bin === pack P  ' => hex2bin( implode( array_reverse( str_split( $hex, 2 ) ) ) ) === pack( 'P', hexdec( $hex ) ) ? 'yes' : 'no', // works for HEX < "8000000000000000"
    ] );
    echo "\n";
}
