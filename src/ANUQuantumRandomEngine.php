<?php
namespace DKZR;

/**
 * \Random\Engine implementation using ANU Quantum Numbers based on work by Setrak Balian (in Python)
 */
final class ANUQuantumRandomEngine implements \Random\Engine
{
    private string $aqn_api_key;
    private array $hex_rand_buffer = [];

    public function __construct( string $aqn_api_key = '' )
    {
        if ( empty( $aqn_api_key ) ) {
            throw new \Random\RandomException( 'The AQN API KEY is required, see https://quantumnumbers.anu.edu.au/pricing for more information.' );
        }
        $this->aqn_api_key = $aqn_api_key;
    }

    public function generate(): string
    {
        if ( empty( $this->hex_rand_buffer ) ) {
            $this->fill_hex_rand_buffer();
        }
        $rand_hex = array_pop( $this->hex_rand_buffer );

        //return hex2bin( $rand_hex ); // makes big endian int
        return hex2bin( implode( array_reverse( str_split( $rand_hex, 2 ) ) ) ); // block-reverse the hex makes little endian int
    }

    private function fill_hex_rand_buffer()
    {
        $this->hex_rand_buffer = $this->anu_request();
    }

    private function anu_request()
    {
        $stream_options = [
            'http' => [
                'method' => 'GET',
                'header' => "x-api-key: {$this->aqn_api_key}\n",
            ],
        ];
        $context = stream_context_create( $stream_options );

        $response = @file_get_contents( 'https://api.quantumnumbers.anu.edu.au/?length=1024&type=hex16&size=4', false, $context );
        $json = json_decode( $response, true );

        if ( json_last_error() !== JSON_ERROR_NONE ) {
            throw new \Random\RandomException( 'Error calling api.quantumnumbers.anu.edu.au. Is your AQN API KEY correct? See https://quantumnumbers.anu.edu.au/pricing for more information.' );
        }

        if ( empty( $json['success'] ) || empty( $json['data'] ) ) {
            if ( isset( $json['message'] ) ) {
                throw new \Random\RandomException( sprintf( 'Error calling api.quantumnumbers.anu.edu.au with message: "%s"', $json['message'] ) );
            } else {
                throw new \Random\RandomException( 'Error calling api.quantumnumbers.anu.edu.au' );
            }
        }

        return $json['data'];
    }
}
