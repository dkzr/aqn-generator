# PHP \Random\Engine implementation for Australian National University Quantum Numbers API

Based on work by [Setrak Balian](https://github.com/sbalian/quantum-random) in Python.

Use the PHPv82+ [`\Random\Randomizer`](https://www.php.net/manual/en/class.random-randomizer.php) class with real quantum random numbers from
[ANU](https://quantumnumbers.anu.edu.au). The default pseudo-random generator is replaced by calls to
the ANU API.

## ANU API KEY required

Get your API KEY at https://quantumnumbers.anu.edu.au/pricing.

**Please note**  
The free licence currently only allows for *100 requests per month* (and 1 request per second).

## Usage

```php
$r = new \Random\Randomizer(
    new \DKZR\ANUQuantumRandomEngine( $anu_api_key )
);

echo "Lucky Number: ", $r->getInt(0, 99), "\n";
```

## License

See [LICENCE](./LICENSE).

### Inspiration

A podcast by [Hens Zimmerman](https://www.buzzsprout.com/2096278) pointed me to the ANU service and the Python library. Thank you!
